# INSTALL
```
$ pkg install python
$ pkg install git
$ pkg install mechanize
$ pip install requests
$ pip install requests bs4
```
# DOWNLOAD
```
git clone https://github.com/BOY-H4MZ4H/enc-lambda
```
# RUN
```
cd enc-lambda
ls
python lambda.py
```
